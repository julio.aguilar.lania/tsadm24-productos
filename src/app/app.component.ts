import { Component } from '@angular/core';
import { RouterLink, RouterModule, RouterOutlet } from '@angular/router';
import { ProductosComponent } from './productos/productos.component';
import { FormaProductoComponent } from './productos/forma-producto/forma-producto.component';
import { InicioComponent } from './inicio/inicio.component';
import { routes } from './app.routes';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterLink, RouterOutlet, InicioComponent, ProductosComponent, FormaProductoComponent],
  templateUrl: './app.component.html',
  //template:'<h2>{{ title }}</h2>',
  styleUrl: './app.component.css'
  //styles:'body { }'
})
export class AppComponent {
  title = 'tienda';
  nombre = 'Julio';
}
