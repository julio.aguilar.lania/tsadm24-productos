import { Routes } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { ProductosComponent } from './productos/productos.component';
import { FormaProductoComponent } from './productos/forma-producto/forma-producto.component';

export const routes: Routes = [
    {
        path: '',
        component: InicioComponent,
    },
    {
        path: 'productos',
        component: ProductosComponent
    },
    {
        path: 'crear_producto',
        component: FormaProductoComponent
    }
];
