import { Component } from "@angular/core";
import { Producto } from "../producto.model";
import { FormsModule } from "@angular/forms";
import { Router } from "@angular/router";
import { ProductosRemotosService } from "../productos-remotos.service";

@Component({
    selector:'app-forma-producto',
    standalone:true,
    imports:[FormsModule],
    providers:[ProductosRemotosService],
    templateUrl:'./forma-producto.component.html',
    styleUrl:'./forma-producto.component.css'
}
)
export class FormaProductoComponent {
    titulo : string = 'Nuevo producto';
    producto: Producto = {idProducto:-1,nombre:'',fechaIngreso:'',tipoProducto:{tipo:'LOCAL'}};

    constructor(private prodService:ProductosRemotosService, private router: Router) {}

    agregarProducto() {
        console.log('agregar producto');
        if (this.producto.nombre.length > 0) { // TODO: Cambiar por validacion mas robusta
            this.prodService.agregarProducto(this.producto)
                .subscribe({
                    next:obj => { 
                        console.log('nuevo');
                        this.router.navigate(['productos']);
                    },
                    error:error => console.log('ERROR', error)
                });
        }
    }
}