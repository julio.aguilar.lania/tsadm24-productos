import { TipoProducto } from "./tipo_producto.model";

export interface Producto {
    idProducto: number;
    nombre: string;
    descripcion?: string;
    fechaIngreso: string;
    tipoProducto: TipoProducto;
}