import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Producto } from './producto.model';
import { ProductosRemotosService } from './productos-remotos.service';

@Component({
  selector: 'app-productos',
  standalone: true,
  imports: [CommonModule],
  providers: [ProductosRemotosService],
  templateUrl: './productos.component.html',
  styleUrl: './productos.component.css'
})
export class ProductosComponent  implements OnInit {
  listaProductos: Producto[] = [];
  //listaProductos? : Producto[] = undefined;

  constructor(private servProductos:ProductosRemotosService) { }

  ngOnInit(): void {
    console.log('Productos ngOnInit')
    //this.listaProductos = this.servProductos.consultarProductos();
    this.servProductos.consultarProductos()
      .subscribe(lista => {
        console.log('Productos recibidos');
        this.listaProductos = lista;
      });
      console.log('Observable recibido');
  }

}
