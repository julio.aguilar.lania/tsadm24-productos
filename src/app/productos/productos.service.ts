import { Injectable } from "@angular/core";
import { Producto } from "./producto.model";


@Injectable()
export class ProductosService {
    static listaGlobalProductos : Producto[] = [
        {
          idProducto: 1,
          nombre: 'Producto Uno',
          fechaIngreso:'2024-03-16',
          tipoProducto: { tipo:"LOCAL"},
          descripcion: 'Esta es una primera descripcion'
        },
        {
          idProducto: 4,
          nombre: 'Producto Cuatro',
          fechaIngreso:'2024-02-16',
          tipoProducto: { tipo:"NACIONAL"}
        }
      ];

    consultarProductos() : Producto[] {
        return ProductosService.listaGlobalProductos;
    }

    agregarProducto(nuevoProducto: Producto) {
        console.log('ProductosService:agregarProducto');
        ProductosService.listaGlobalProductos.push(nuevoProducto);
        console.log(ProductosService.listaGlobalProductos.length + ' productos');
    }
}